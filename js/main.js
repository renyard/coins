(function() {
    "use strict";

    var $form = $('#coinsForm'),
        $input = $('#pennies'),
        $result = $('#result'),
        coinMap = {
            '200': '\u00A32',
            '100': '\u00A31',
            '50': '50p',
            '20': '20p',
            '2': '2p',
            '1': '1p'
        };

    $form.submit(function(e) {
        var requiredCoins,
            pennies,
            markup = '';

        e.preventDefault();

        pennies = $input.val();
        requiredCoins = coins.calc(pennies);

        for (var key in requiredCoins) {
            if (!requiredCoins.hasOwnProperty(key)) {
                continue;
            }

            markup += '<li>';
            markup += requiredCoins[key] + ' x ' + coinMap[key];
            markup += '</li>';

            $result.html(markup);
        }
    });
})();
