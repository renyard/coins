var coins = (function() {
    "use strict";

    var module = {};

    module.calc = function(input) {
        var pence = module._getPence(input),
            // Support £1, 50p, 20p, 2p & 1p.
            // Do not use 10p & 5p.
            denominations = [200, 100, 50, 20, 2, 1],
            currCoin,
            numOfCurrCoins,
            coins = {};

            // Validate input.
            if (!/^(:?\u00A3)?[0-9]*\.?[0-9]*p?$/.test(input)) {
                return {};
            }

            // Loop through denominations,
            // calculating required coins.
            for (var i = 0; i < denominations.length; i++) {
                // Stop if no more coins are required.
                if (pence === 0) {
                    break;
                }

                currCoin = denominations[i];

                // devide pence by value of current denomination.
                numOfCurrCoins = parseInt(pence / currCoin);
                if (numOfCurrCoins) {
                    coins[currCoin] = numOfCurrCoins;
                }

                // Count down how many pence remain.
                pence = pence % currCoin;
            }

            return coins;
    };

    module._getPence = function(input) {
        // Get pounds to add to our pence later.
        var pounds = module._getPounds(input),
            pence = 0, // Default to 0.
            // One or more digits, optinal p.
            penceOnlyRegexp = /^([0-9]+)p?$/,
            // Optional pound sign, pound value, optional p,
            // capture pence after decimal point.
            poundsRegexp = /[0-9]*\.([0-9]+)p?$/,
            penceOnlyMatch,
            poundsMatch;

        penceOnlyMatch = penceOnlyRegexp.exec(input);
        if (penceOnlyMatch !== null) {
            pence = parseInt(penceOnlyMatch[1]);
        }
        else {
            poundsMatch = poundsRegexp.exec(input);
            if (poundsMatch !== null) {
                pence = parseFloat('0.' + poundsMatch[1]);

                // Round pence.
                pence = Math.round(pence * 100);
            }
        }

        pence = pounds * 100 + pence;
        return pence;
    };

    // Parse input to get number of pounds.
    module._getPounds = function(input) {
        var pounds = 0, // Default to 0.
            // Pounds will be prepended with £
            // or appended with a decimal point.
            poundSignRegexp = /^\u00A3([0-9]+)[.p]?/,
            decimalRegexp = /^([0-9]+)\./,
            poundSignMatch,
            decimalMatch;

        poundSignMatch = poundSignRegexp.exec(input);
        if (poundSignMatch !== null) {
            pounds = poundSignMatch[1];
        }
        else {
            decimalMatch = decimalRegexp.exec(input);
            if (decimalMatch !== null) {
                pounds = decimalMatch[1];
            }
        }

        pounds = parseInt(pounds);
        return pounds;
    };

    return module;
})();
