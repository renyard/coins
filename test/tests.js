(function() {
    "use strict";

    test('_getPounds Positive Tests', function() {
        equal(coins._getPounds('\u00A31'), 1, '1 pound');
        equal(coins._getPounds('1.34'), 1, '1 pound with decimal');
        equal(coins._getPounds('1.34p'), 1, '1 pound with decimal and pence');
        equal(coins._getPounds('\u00A31.'), 1, '1 pound with pounds and decimal point');
        equal(coins._getPounds('1.'), 1, '1 pound with decimal point only');
    });

    test('_getPounds Negative Tests', function() {
        equal(coins._getPounds('10p'), 0, '10 pence');
        equal(coins._getPounds('25'), 0, '25 pence inferred');
        equal(coins._getPounds('\u00A30.50'), 0, '0 pounds, 50 pence');
        equal(coins._getPounds('\u00A3.50'), 0, '0 pounds, 50 pence');
        equal(coins._getPounds('1x'), 0, 'Non numeric character');
        equal(coins._getPounds('\u00A3p'), 0, 'Symbols only');
        equal(coins._getPounds(''), 0, 'Empty string');
    });

    test('_getPence Positive Tests', function() {
        equal(coins._getPence('4'), 4, 'Single digit');
        equal(coins._getPence('85'), 85, 'Double digit');
        equal(coins._getPence('197p'), 197, 'Pence symbol');
        equal(coins._getPence('2p'), 2, 'Pence symbol single digit');
        equal(coins._getPence('1.87'), 187, 'Pounds decimal');
        equal(coins._getPence('\u00A31p'), 100, 'Missing pence');
        equal(coins._getPence('\u00A31.p'), 100, 'Missing pence but present decimal point');
        equal(coins._getPence('001.41p'), 141, 'Buffered zeros');
        equal(coins._getPence('4.235p'), 424, 'Rounding three decimal places to two');
        equal(coins._getPence('\u00A31.257422457p'), 126, 'Rounding with symbols');
    });

    test('_getPence Negative Tests', function() {
        equal(coins._getPence(''), 0, 'Empty string');
        equal(coins._getPence('1x'), 0, 'Non-numeric character');
        //equal(coins._getPence('\u00A31x.0p'), 0, 'Non-numeric character');
        equal(coins._getPence('\u00A3p'), 0, 'Missing digits');
    });

    test('calc Positive Tests', function() {
        deepEqual(coins.calc('4'), {'2': 2}, 'Single digit');
        deepEqual(coins.calc('85'), {'50': 1, '20': 1, '2': 7, '1': 1}, 'Double digit');
        deepEqual(coins.calc('197p'), {'100': 1, '50': 1, '20': 2, '2': 3, '1': 1}, 'Pence symbol');
        deepEqual(coins.calc('2p'), {'2': 1}, 'Pence symbol single digit');
        deepEqual(coins.calc('1.87'), {'100': 1, '50': 1, '20': 1, '2': 8, '1': 1}, 'Pounds decimal');
        deepEqual(coins.calc('\u00A31p'), {'100': 1}, 'Missing pence');
        deepEqual(coins.calc('\u00A31.p'), {'100': 1}, 'Missing pence but present decimal point');
        deepEqual(coins.calc('001.41p'), {'100': 1, '20': 2, '1': 1}, 'Buffered zeros');
        deepEqual(coins.calc('4.235p'), {'200': 2, '20': 1, '2': 2}, 'Rounding three decimal places to two');
        deepEqual(coins.calc('\u00A31.257422457p'), {'100': 1, '20': 1, '2': 3}, 'Rounding with symbols');
    });

    test('calc Negative Tests', function() {
        deepEqual(coins.calc(''), {}, 'Empty string');
        deepEqual(coins.calc('1x'), {}, 'Non-numeric character');
        deepEqual(coins.calc('\u00A31x.0p'), {}, 'Non-numeric character');
        deepEqual(coins.calc('\u00A3p'), {}, 'Missing digits');
    });
})();
